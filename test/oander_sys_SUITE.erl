-module(oander_sys_SUITE).
-include_lib("common_test/include/ct.hrl").
-export([all/0, init_per_testcase/2, end_per_testcase/2, 
    test_registered_processes/1, test_persistent_storage/1, test_access_by_pid/1,
    test_process_restart/1]).

-define(prefixed(P), list_to_atom("oander_db_" ++ atom_to_list(P))).


all() -> [test_registered_processes, test_persistent_storage, test_access_by_pid, test_process_restart].

init_per_testcase(_, Config) ->
    application:load(oander),    
    application:set_env(oander, storage_path, "/tmp"),
    application:set_env(oander, databases, [test_db]),
    {ok, _} = application:ensure_all_started(oander),
    Config.

end_per_testcase(_, _) ->
    application:stop(oander),
    file:delete("/tmp/test_db"),
    ok.

test_registered_processes(_) ->     
    true = lists:member(?prefixed(test_db), registered()).


test_persistent_storage(_) -> 
    ok = oander:set(test_db, <<1>>, "value"),
    application:stop(oander),
    {ok, _} = application:ensure_all_started(oander),
    {ok, "value"} = oander:get(test_db, <<1>>).


test_access_by_pid(_) ->    
    Key = <<1>>,
    Value = 42,
    Pid = whereis(?prefixed(test_db)),
    oander:set(Pid, Key, Value),
    {ok, Value} = oander:get(Pid, Key),
    oander:del(Pid, Key),
    notfound = oander:get(Pid, Key).

test_process_restart(_) ->
    P1 = whereis(?prefixed(test_db)),
    exit(P1, kill),
    timer:sleep(500), % hacky, hacky..... :/
    P2 = whereis(?prefixed(test_db)),
    true = is_pid(P2),
    true = P1 =/= P2.


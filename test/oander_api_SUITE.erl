-module(oander_api_SUITE).
-include_lib("common_test/include/ct.hrl").
-export([suite/0, init_per_suite/1, end_per_suite/1, all/0]).
-export([get_nonexisting_key/1, get_existing_key/1,
        get_nonexisting_key_with_default_value/1, get_existing_key_with_default_value/1,
        delete_existing_key/1, delete_nonexisting_key/1,
        test_nonexistent_database/1, 
        get2_non_binary_keys/1,
        get3_non_binary_keys/1,
        set_non_binary_keys/1,
        del_non_binary_keys/1]).
 
suite() ->    
    [].

init_per_suite(Config) ->         
    application:load(oander),    
    application:set_env(oander, storage_path, "/tmp"),
    application:set_env(oander, databases, [test_db]),
    {ok, _} = application:ensure_all_started(oander),
    Config.

end_per_suite(_Config) ->    
    ok = application:stop(oander),
    file:delete("/tmp/test_db").

all() -> [
    get_nonexisting_key, 
    get_existing_key,
    get_nonexisting_key_with_default_value,
    get_existing_key_with_default_value,
    delete_existing_key,
    delete_nonexisting_key,
    test_nonexistent_database,
    get2_non_binary_keys,
    get3_non_binary_keys,
    set_non_binary_keys,
    del_non_binary_keys
].
 

get_nonexisting_key(_) ->
    notfound = oander:get(test_db, <<1,2,3,4,5,6>>).

get_existing_key(_) ->
    ok = oander:set(test_db, <<1>>, "a"),
    {ok, "a"} = oander:get(test_db, <<1>>).

get_nonexisting_key_with_default_value(_) -> 
    {ok, default_value} = oander:get(test_db, <<1,2,3>>, default_value).
 
get_existing_key_with_default_value(_) ->
    ok = oander:set(test_db, <<2>>, "a"),
    {ok, "a"} = oander:get(test_db, <<2>>, default_value).

delete_existing_key(_) ->
    ok = oander:set(test_db, <<3>>, 1),
    {ok, 1} = oander:get(test_db, <<3>>),
    ok = oander:del(test_db, <<3>>),
    notfound = oander:get(test_db, <<3>>).

delete_nonexisting_key(_) ->    
    notfound = oander:get(test_db, <<42>>),
    ok = oander:del(test_db, <<42>>).

test_nonexistent_database(_) ->
    {'EXIT', {noproc, _}} = (catch oander:get(wrong_db_name, <<1>>)),
    {'EXIT', {noproc, _}} = (catch oander:set(wrong_db_name, <<1>>, "Value")),
    {'EXIT', {noproc, _}} = (catch oander:del(wrong_db_name, <<1>>)),
    {'EXIT', {noproc, _}} = (catch oander:get(wrong_db_name, <<1>>, default)),
    ok.
        
get2_non_binary_keys(_) ->
    Keys = [1, "string", atom, 1.0],
    lists:foreach(fun (K) -> {'EXIT', {function_clause, _}} = (catch oander:get(test_db, K)) end, Keys),
    ok.

get3_non_binary_keys(_) ->
    Keys = [1, "string", atom, 1.0],
    lists:foreach(fun (K) -> {'EXIT', {function_clause, _}} = (catch oander:get(test_db, K, 0)) end, Keys),
    ok.

set_non_binary_keys(_) ->
    Keys = [1, "string", atom, 1.0],
    lists:foreach(fun (K) -> {'EXIT', {function_clause, _}} = (catch oander:set(test_db, K, value)) end, Keys),
    ok.

del_non_binary_keys(_) ->
    Keys = [1, "string", atom, 1.0],
    lists:foreach(fun (K) -> {'EXIT', {function_clause, _}} = (catch oander:del(test_db, K)) end, Keys),
    ok.

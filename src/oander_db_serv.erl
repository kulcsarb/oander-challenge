%%%-------------------------------------------------------------------
%% @doc gen_server implementing a simple key-value store
%% @end
%%%-------------------------------------------------------------------
-module(oander_db_serv).
-behaviour(gen_server).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-export([start_link/2, set/3, get/3, get/2, del/2]).
-include_lib("eunit/include/eunit.hrl").
-include("oander.hrl").
-define(testCase(Fun, Args), fun (T) -> Fun(T, Args) end).  % intentionally following eunit's "assert*" naming pattern


%%
%% -------- EXTERNAL API -------
%%

-spec start_link(Name::atom(), Path::list()) -> {ok, pid()}.
start_link(DatabaseName, Path) ->    
    ProcessName = prefixed(DatabaseName),
    lager:debug("Starting GenServer ~p for database ~p~n", [ProcessName, DatabaseName]),
	gen_server:start_link({local, ProcessName}, ?MODULE, [DatabaseName, Path], []).

-spec get(Ref :: pid() | atom(), Key :: <<>>, Default :: any()) -> {ok, any()}.
get(Ref, Key, Default) when is_binary(Key) ->         
    gen_server:call(prefixed(Ref), {get, Key, Default}).

-spec get(Ref :: pid() | atom(), Key :: <<>>) -> {ok, any()} | notfound.
get(Ref, Key) when is_binary(Key) ->     
    gen_server:call(prefixed(Ref), {get, Key}).

-spec set(Ref :: pid() | atom(), Key :: <<>>, Value :: any()) -> ok.
set(Ref, Key, Value) when is_binary(Key) ->
    gen_server:call(prefixed(Ref), {set, Key, Value}).

-spec del(Ref :: pid() | atom(), Key :: <<>>) -> ok.
del(Ref, Key) when is_binary(Key) -> 
    gen_server:call(prefixed(Ref), {del, Key}).

%%
%% ------ GEN_SERVER CALLBACKS ------
%%

init([Name, StoragePath]) ->
    process_flag(trap_exit, true),
	lager:info("Initializing database ~p at ~p ~n", [Name, StoragePath]),
    FilePath = filename:join([StoragePath, Name]),
    {ok, Table} = dets:open_file(FilePath, []),
	{ok, Table}.


handle_call({get, Key}, _From, Table) ->
    lager:debug("get(~p)", [Key]),
    case dets:lookup(Table, Key) of 
        [] -> {reply, notfound, Table};
        [{Key, Value}] -> {reply, {ok, Value}, Table}
    end;


handle_call({get, Key, Default}, _From, Table) ->
    lager:debug("get(~p, ~p)", [Key, Default]),
    case dets:lookup(Table, Key) of 
        [] -> {reply, {ok, Default}, Table};
        [{Key, Value}] -> {reply, {ok, Value}, Table}
    end;


handle_call({set, Key, Value}, _From, Table) ->
    lager:debug("set(~p, ~p)", [Key, Value]),
    dets:insert(Table, {Key, Value}),    
    {reply, ok, Table};


handle_call({del, Key}, _From, Table) ->    
    lager:debug("del(~p)", [Key]),
    dets:delete(Table, Key),    
    {reply, ok, Table}.


handle_cast(_, Table) ->
    {noreply, Table}.


handle_info(Msg, Table) ->
    lager:warning("Unexpected message received: ~p~n", [Msg]),
    {noreply, Table}.


terminate(Reason, Table) ->
    lager:info("~p received, terminating database process ~p~n", [Reason, Table]),    
    dets:close(Table).
	

code_change(_, Table, _) ->
	{ok, Table}.


%%
%% ------ INTERNAL FUNCTIONS ------
%%

prefixed(Name) when is_atom(Name) -> list_to_atom(?PROCESS_PREFIX ++ atom_to_list(Name));
prefixed(X) -> X.


%%
%%   --------------------  UNIT TESTS -------------------
%%

prefixed_test_() ->
    [   
        {"must return an atom() parameter prefixed with the value stored in ?PROCESS_PREFIX", 
            ?_assertEqual(list_to_atom(?PROCESS_PREFIX ++ "valami"), prefixed(valami))},
        {"return parameter as received", 
            ?_assertEqual(prefixed(self()), self())},
        {"return parameter as received",
            ?_assertEqual("A", prefixed("A"))},
        {"return parameter as received",
            ?_assertEqual(1, prefixed(1))}
    ].


setup() ->
    {ok, T} = dets:open_file(test_table, [{auto_save, infinity}, {ram_file, true}]),
    T.

teardown(T) ->
    dets:close(T),
    file:delete(T).


handle_call_get3_key_notexists_test_() ->
    Get = fun(T, {K, _V, D}) ->
        ?_assertEqual({reply, {ok, D}, T}, handle_call({get, K, D}, self(), T))
    end,    
    {foreach,
        fun setup/0,
        fun teardown/1,
        [
            ?testCase(Get, {1, 2, default}),
            ?testCase(Get, {"A", "B", default}),            
            ?testCase(Get, {key, <<1,2,3>>, default})
        ]
    }.


handle_call_get3_key_exists_test_() ->
    Get = fun(T, {K, V, D}) ->        
        dets:insert(T, {K, V}),
        ?_assertEqual({reply, {ok, V}, T}, handle_call({get, K, D}, self(), T))
    end,
    {foreach,
        fun setup/0,
        fun teardown/1,
        [
            ?testCase(Get, {1, 2, default}),
            ?testCase(Get, {"A", "B", default}),            
            ?testCase(Get, {key, <<1,2,3>>, default})
        ]
    }.


handle_call_get2_key_exists_test_() ->
    Get = fun(T, {K, V}) ->        
        dets:insert(T, {K, V}),
        ?_assertEqual({reply, {ok, V}, T}, handle_call({get, K}, self(), T))
    end,
    {foreach,
        fun setup/0,
        fun teardown/1,
        [
            ?testCase(Get, {1, 2}),
            ?testCase(Get, {"A", "B"}),            
            ?testCase(Get, {key, <<1,2,3>>})
        ]
    }.


handle_call_get2_key_notexists_test_() ->
    Get = fun(T, K) ->        
        ?_assertEqual({reply, notfound, T}, handle_call({get, K}, self(), T))
    end,
    {foreach,
        fun setup/0,
        fun teardown/1,
        [ 
            ?testCase(Get, 1),
            ?testCase(Get, key),
            ?testCase(Get, <<3>>)
        ]
    }.


handle_call_set_test_() ->
    Set = fun(T, {K, V}) ->
        {reply, ok, T} = handle_call({set, K, V}, self(), T),
        ?_assertEqual([{K, V}], dets:lookup(T, K))
    end,
    {foreach, 
        fun setup/0,
        fun teardown/1,        
        [
            ?testCase(Set, {key, value}),
            ?testCase(Set, {1, 2}),
            ?testCase(Set, {<<1>>, "B"})            
        ]
    }.


handle_call_del_test_() ->
    Delete = fun(T, {K, V}) -> 
        dets:insert(T, {K, V}),
        {reply, ok, T} = handle_call({del, K}, self(), T),
        ?_assertEqual([], dets:lookup(T, K))
    end,
    {foreach,
        fun setup/0,
        fun teardown/1,
        [ 
            ?testCase(Delete, {key, value}),            
            ?testCase(Delete, {123, "A"}),  
            ?testCase(Delete, {"bbbbb", <<45, 23, 43>>})            
        ]
    }.            


terminate_test() ->
    {setup,
        fun setup/0,
        fun teardown/1,
        fun(T) ->            
            terminate(shutdown, T),
            ?assertError(badarg, fun() -> dets:lookup(T, <<1>>) end)
        end
    }.

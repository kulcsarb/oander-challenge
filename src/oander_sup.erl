%%%-------------------------------------------------------------------
%% @doc oander top level supervisor.
%% @end
%%%-------------------------------------------------------------------
-module(oander_sup).
-behaviour(supervisor).
-export([start_link/1, init/1]).
-include_lib("eunit/include/eunit.hrl").
-define(SERVER, ?MODULE).

%%
%% -------- EXTERNAL API -------
%%

start_link([Databases, StoragePath]) ->
    lager:debug("Starting supervisor"),
    supervisor:start_link({local, ?SERVER}, ?MODULE, [Databases, StoragePath]).


%%
%% ----- SUPERVISOR CALLBACKS -----
%%

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional

init([DatabaseNames, StoragePath]) ->    
    SupFlags = #{strategy => one_for_one,
                 intensity => 5,
                 period => 10},                   
    ChildSpecs = generate_specs(arguments(DatabaseNames, StoragePath)),
    {ok, {SupFlags, ChildSpecs}}.


%%
%% ------ INTERNAL FUNCTIONS ------
%%

arguments(DatabaseNames, StoragePath)-> 
    lists:map(fun(N) -> {N, StoragePath} end, DatabaseNames).

generate_specs(Args) ->
    lists:map(fun childspec/1, Args).

childspec({DatabaseName, StoragePath}) ->    
    {DatabaseName, {oander_db_serv, start_link, [DatabaseName, StoragePath]}, permanent, 2000, worker, [oander_db_serv]}.


%%
%%   --------------------  UNIT TESTS -------------------
%%

arguments_test_() ->
    ?_assertEqual([{"a", "/path"}, {"b", "/path"}], arguments(["a", "b"], "/path")).

generate_specs_test_() -> 
    ?_assertMatch(
        [
            {"a", {_, _, ["a", "/path"]}, _, _, _, _}, 
            {"b", {_, _, ["b", "/path"]}, _, _, _, _}
        ],    
        generate_specs([{"a", "/path"}, {"b", "/path"}])
    ).

childspec_test_() ->
    ?_assertMatch({"Name", {_, _, ["Name", "/path"]}, _, _, _, _}, childspec({"Name", "/path"})).
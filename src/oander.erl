%%%-------------------------------------------------------------------
%% @doc oander public API
%% @end
%%%-------------------------------------------------------------------
-module(oander).
-behaviour(application).
-export([start/2, stop/1, get/3, get/2, set/3, del/2]).
-include_lib("eunit/include/eunit.hrl").


%% -------- EXTERNAL API -------

-spec get(Ref :: pid() | atom(), Key :: <<>>, Default :: any()) -> {ok, any()}.
get(Ref, Key, Default) when is_binary(Key) -> 
    oander_db_serv:get(Ref, Key, Default).

-spec get(Ref :: pid() | atom(), Key :: <<>>) -> {ok, any()} | notfound.
get(Ref, Key) when is_binary(Key) -> 
    oander_db_serv:get(Ref, Key).

-spec set(Ref :: pid() | atom(), Key :: <<>>, Value :: any()) -> ok.
set(Ref, Key, Value) when is_binary(Key) ->
    oander_db_serv:set(Ref, Key, Value).

-spec del(Ref :: pid() | atom(), Key :: <<>>) -> ok.
del(Ref, Key) when is_binary(Key) -> 
    oander_db_serv:del(Ref, Key).


%% --- APPLICATION CALLBACKS ---

start(_StartType, _StartArgs) ->
    %% Keep only valid database names, as specified...
    lager:debug("Starting application oander "),
    Databases = validate(database_names()),
    Path = storage_path(),    
    lager:info("Storage path: ~p~n", [Path]),
    lager:info("Configured databases: ~p~n", [Databases]),    
    oander_sup:start_link([Databases, Path]).

stop(_State) ->
    ok.


%%
%% ------ INTERNAL FUNCTIONS ------
%%

storage_path() -> application:get_env(oander, storage_path, "/tmp").
database_names() -> application:get_env(oander, databases, []).
validate(Names) -> lists:filter(fun erlang:is_atom/1, Names).


%%
%%   --------------------  UNIT TESTS -------------------
%%

validate_test_() ->
    {"should return a list, containing elements of atom type from the original",
        ?_assertEqual([a, b, c], validate([a, b, 1, "ketto", <<21>>, c]))
    }.

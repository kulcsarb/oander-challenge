Oander
=====

An coding challange solution implemented by Balazs Kulcsar for OANDER Development Kft,

Software Versions
-----------------
    Erlang/OTP 23
    Rebar 3.13.2

Configuration
-------------
    The application can be configured through its app.src file, which is found in the src directory.
    First, set the `storage_path` option to any directory you wish, and then define your databases with the `databases` key

        {env, [
            {storage_path, "/tmp"},    
            {databases, [johnny_bravo, dexter]}
        ]},

    To configure logging, (changing levels, colors, handlers, etc), please refer to `config/sys.config` file

Usage
-----
    This Erlang application is meant to be used from the erlang shell, in development mode.

    $ rebar3 shell
    > oander.set(johnny_bravo, <<1>>, "Man, I'm pretty").
    ok
    > oander.get(johnny_bravo, <<1>>).
    {ok, "Man, I'm pretty"}    
    > oander.del(johnny_bravo, <<1>>).
    ok
    > oander.get(johnny_bravo, <<1>>).
    notfound
    > oander.get(dexter, <<1>>).
    notfound    
    > oander.get(dexter, <<1>>, "This is a good day for science!").
    {ok, "This is a good day for science!"}
            
    before exiting the vm, please stop the application by running
    > application:stop(oander) 
    
    from the shell. Shutting down the appliction by aborting the vm probably will leave the database file corrupted.


Tests
-----
    The application contain both unit and integration tests, both are available through rebar:

    $ rebar eunit
    $ rebar ct


CHANGELOG
=========
2020.07.06, approx 3h 
 - creating a rebar project
 - writing a simple gen_server 
 - implementing the key-value store functionality using dets
 - creating an api for the genserver with typespecs

2020.07.07, approx 2h 
 - include lager as dependency
 - moving database names to the env key in the application descriptor file
 - reading database names from application env, and type chacking them in the main application file
 - declared the main API for the appliction in oander.erl 
 - catch exit signals and properly close DETS tables when server processes exit

2020.07.08, approx 4h
 - made the storage path for DETS tables configurable, so they wont clutter the current directory 
 - refactoring oander.erl, and oander_sup to a more functional style
 - included lager, configured logging, tried different color variations and message formats, and inserted logger calls in the code
 - more refactorings
 - prefixed gen_serv processes to avoid possible name clashes 


 2020.07.08, approx 6h
  - writing extensive unit tests
  - writing two common test suite
  - some manual code linting, double checking 


Total work time: approx 17h
